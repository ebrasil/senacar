﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SENACAR.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DescricaoView : ContentPage
	{
		public DescricaoView (Servico servico)
		{
			InitializeComponent ();

            this.Title = servico.Nome;
		}

        private void ButtonProximo_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new AgendamentoView());
        }
    }
}