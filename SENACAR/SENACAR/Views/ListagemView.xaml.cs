﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SENACAR.Views
{
    public class Servico
    {
        public string Nome { get; set; }

        public float Preco { get; set; }

        public string PrecoFormatado
        {
            get { return string.Format("R$ {0}", Preco); }
        }
    }
    public partial class ListagemView : ContentPage
    {
        public List<Servico> Servicos { get; set; }
        public ListagemView()
        {
            InitializeComponent();

            this.Servicos = new List<Servico>
            {
                new Servico {Nome = "Chevrolet Onix", Preco = 44000},
                new Servico {Nome = "Hyunday HB20", Preco = 42000},
                new Servico {Nome = "Renault Sandero", Preco = 39000},
                new Servico {Nome = "Ford Fiesta", Preco = 29000},
                new Servico {Nome = "Honda Civic", Preco = 84000},
            };

            this.BindingContext = this;

            //ListViewServicos.ItemsSource = this.Servicos;
            //{
            //    "Chevrolet Onix",
            //    "Hyunday HB20",
            //    "Renault Sandero",
            //    "Ford Fiesta",
            //    "Honda Civic"
            //};
        }

        

        private void ListViewServicos_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var servico = (Servico)e.Item;

            Navigation.PushAsync(new DescricaoView(servico));

            //DisplayAlert("Serviço",
            //    string.Format("Você selecionou o serviço '{0}'. Valor: {1}",
            //    servico.Nome, servico.PrecoFormatado, "OK"));
        }

       
    }
}
